# Calibration ifo configuration: H1

This repo holds calibration configuration files for the Hanford
observatory "H1" detector.

The primary file is the
[pydarm](https://git.ligo.org/Calibration/pydarm) calibration model
configuration .ini file for H1, starting after O3.

