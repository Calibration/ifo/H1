from pydarm.cmd._report import Report
# from pydarm.cmd._log import logger
import numpy as np
from pydarm.plot import BodePlot
import matplotlib.pyplot as plt
import os

# report_hot = Report.find('20230628T015112Z')
# report_cold = Report.find('20230716T034950Z')

if __name__ == "__main__":
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument('-r', '--report', type=str,
                   help='path to report')
    p.add_argument('-o', type=str,
                   help='path to output file')
    args = p.parse_args()

    fout_stub, fout_ext = os.path.splitext(args.o)
    report = Report.find(args.report)
    # import ipdb;ipdb.set_trace()
    report.load_measurements()
    processed_sensing = report.measurements['Sensing'][0]
    freqs, resp, unc = processed_sensing.get_processed_measurement_response()

    bp = BodePlot()
    bp.plot(freqs, resp)
    bp.save(f'{fout_stub}_quickbodeplot.png')

    dout = np.zeros((len(freqs), 4), dtype=float)
    dout[:, 0] = freqs
    dout[:, 1] = resp.real
    dout[:, 2] = resp.imag
    dout[:, 3] = unc

    np.savetxt(args.o, dout, header='Frequency (Hz) Response(Real) Response(Imag) Uncertainty')
